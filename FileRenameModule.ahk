; File Rename Module
{
	; Variables Used
	; --------------
	; A_WorkingDir		A built in variable in AHK for the current directory the
	;					program exists in.
	;
	; AccountNumber		Is grabbed from the StringRight function. Should be 7 digits.
	;
	; ShortDirectory	A single variable unit to shorten the long path of the files
	;					being renamed.
	
	; Gets us the Service Number
	StringRight, AccountNumber, A_WorkingDir, 7
	
	; Shortens the Directory so it's not the text beyond the =
	; Will display F:\Other\Test\1234567\1234567
	; Essentially a shorter string.
	ShortDirectory = %A_WorkingDir%\%AccountNumber%
	
	; Renames the file to the ShortDirectory, the document type, and the version number.
	FileMove, %ShortDirectory% CAD *.pdf, %ShortDirectory% CAD %VersionNumber%.pdf
	
	; For debugging, displays the variables.
	MsgBox, %AccountNumber%
	MsgBox, %VersionNumber%
	MsgBox, %ShortDirectory%
}