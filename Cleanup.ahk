; File Rename Module
Cleanup:
{
	GoSub, GetVariables
	GoSub, CreateFolders
	GoSub, CopyToBackup
	MsgBox, DesignType = %DesignType%
	GoSub, TestDesignType
	GoSub, Debug
	return
}

GetVariables:
{
	; Variables log
	; --------------
	; A_WorkingDir		A built in variable in AHK for the current directory the
	;					program exists in.
	;
	; AccountNumber		Is grabbed from the StringRight function. Should be 7 digits.
	; 
	; DesignType		A variable used to define the type of design the project is.
	;					Part 1, Part 2, or Proposal.
	; 
	; VersionNumber		A variable used to define the version number in folder naming.
	;					for this process, we want to use the CAD Object Ver. Number.
	; 
	; ShortDirectory	A variable "alias" for the working directory.
	; 
	; ShortBackupDir	A variable "alias" for the backup directory path.
	; ShortDesignDir	A variable "alias" for the design type directory.
	
	
	; Gets us the Service Number
	StringRight, AccountNumber, A_WorkingDir, 7
	
	; Opens an input box for the user.
	; For debugging - Sets DesignType.
	; Inputbox, VariableStored, MessagePrompt
	InputBox, DesignType, Debugging Design Type
	
	; Opens an input box for the user.
	; For debugging - Sets VersionNumber.
	; Inputbox, VariableStored, MessagePrompt
	InputBox, VersionNumber, Debugging Version Number
	
	; Shortens the Directory so it's not the text beyond the =
	; Will display F:\Other\Test\1234567\1234567
	; Essentially a shorter string.
	ShortDirectory = %A_WorkingDir%\%AccountNumber%

	; ShortestDirectory Variable
	sd = %A_WorkingDir%\%AccountNumber%

	; Shortening the Backup into a declared variable.
	ShortBackupDir = %sd% BACKUP

	; Shortest Backup Variable
	sbd = %sd% BACKUP

	; Shortening the Design Type directory into a declared variable.
	ShortDesignDir = %sd% %DesignType% %VersionNumber%

	; Shortest Design Directory Variable.
	sdd = %sd% %DesignType% %VersionNumber%
	return
}

CreateFolders:
{
	; Creates the backup directory. For restoring if mistakes are present.
	FileCreateDir, %ShortBackupDir%

	; Creates the design type directory, to eventually be zipped.
	FileCreateDir, %ShortDesignDir%
	return
}

CopyToBackup:
{
	; Copies everything in the working directory to the created
	; file backup directory.
	FileCopy, %A_WorkingDir%\*.*, %ShortDirectory% BACKUP
	return
}

TestDesignType:
{
	MsgBox, DesignType = %DesignType%
	IfInString, DesignType, Proposal ; THIS WORKS!!!
		GoSub, CopyToProposal
	if (%DesignType% = "Proposal")
	{
		GoSub, CopyToProposal
	}
	else if (%DesignType% = "Part 1")
	{
		GoSub, CopyToCP
	}
	else if (%DesignType% = "Part 2")
	{
		GoSub, CopyToCAD
	}
	else if (%DesignType% = "")
	{
		MsgBox No Design Type.
		exit
	}
}

CopyToProposal:
{
	FileCopy, %sd% CP *.pdf, %sdd%
	FileCopy, %sd% [pPrRoOpPoOsSaAlL] *.pdf, %sdd%
	FileCopy, %sd% [sSuUrRvVeEyY] *.pdf, %sdd%
	FileCopy, %sd% [pPrRoOpPoOsSaAlL cCaAlLcC] *.pdf, %sdd%
	return
}

CopyToCP:
{

}

CopyToCAD:
{

}

Debug:
{
	; For debugging, displays the variables.
	MsgBox,,Debug AccountNumber, %AccountNumber%
	MsgBox,,Debug DesignType, %DesignType%
	MsgBox,,Debug VersionNumber, %VersionNumber%
	MsgBox,,Debug ShortDirectory, %ShortDirectory%
}