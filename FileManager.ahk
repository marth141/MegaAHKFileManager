;Gui, Tab, Tab1
Gui, Add, Tab, x6 y13 w490 h380 , Tab1|Tab2
Gui, Add, Text, x26 y33 w220 h30 , CAD File Manager
Gui, Add, Button, x26 y133 w130 h30 , Update Version Number
Gui, Add, Button, x26 y173 w130 h30 , Clean 1 Account
Gui, Add, Button, x26 y213 w130 h30 , Clean 3 Accounts
Gui, Add, Button, x26 y313 w130 h40 , Copy To Gemini Sync Folder
Gui, Add, Radio, x166 y173 w70 h30 , Proposal
Gui, Add, Radio, x236 y173 w50 h30 , Part 1
Gui, Add, Radio, x286 y173 w50 h30 , Part 2
Gui, Add, CheckBox, x336 y173 w70 h30 , Traditional
Gui, Add, Text, x176 y213 w60 h20 , Account 1:
Gui, Add, Radio, x236 y213 w70 h30 , Proposal
Gui, Add, Radio, x306 y213 w50 h30 , Part 1
Gui, Add, Radio, x356 y213 w50 h30 , Part 2
Gui, Add, CheckBox, x406 y213 w70 h30 , Traditional
Gui, Add, Text, x176 y243 w60 h20 , Account 2:
Gui, Add, Radio, x236 y243 w70 h30 , Proposal
Gui, Add, Radio, x306 y243 w50 h30 , Part 1
Gui, Add, Radio, x356 y243 w50 h30 , Part 2
Gui, Add, CheckBox, x406 y243 w70 h30 , Traditional
Gui, Add, Text, x176 y273 w60 h20 , Account 2:
Gui, Add, Radio, x236 y273 w70 h30 , Proposal
Gui, Add, Radio, x306 y273 w50 h30 , Part 1
Gui, Add, Radio, x356 y273 w50 h30 , Part 2
Gui, Add, CheckBox, x406 y273 w70 h30 , Traditional
Gui, Add, Button, x166 y313 w130 h40 , Grab Templates
Gui, Add, Text, x116 y73 w70 h20 , CAD Version
Gui, Add, Text, x26 y73 w80 h20 , Service Number
Gui, Add, Edit, x26 y93 w70 h30 , 1234567
Gui, Add, Edit, x116 y93 w50 h30 +Limit2, 1
; Generated using SmartGUI Creator 4.0
Gui, Show, x328 y240 h613 w836, New GUI Window
Return

GuiClose:
ExitApp