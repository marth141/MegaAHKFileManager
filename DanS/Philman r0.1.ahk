#SingleInstance Force
#NoEnv
Menu, Tray, Icon, shell32.dll, 88
Version=0.1

iniread, winX, %A_MyDocuments%\PhilmanSettings.ini, Main, xPos, 200
iniread, winY, %A_MyDocuments%\PhilmanSettings.ini, Main, yPos, 200
if (WinX < 0)
	WinX := 200
if (WinY < 0)
	WinY := 200

Gui, add, Tab2, x0 y5 h400 w300, Part One||Part Two
Gui, add, text, x150 y5 BackgroundTrans, Design #:
Gui, add, edit, -E0x200  +center xp+50 yp-1 h20 w20 vDeNum,1

Gui, Tab, Part One
Gui, add, text, x5 y30 BackgroundTrans, REPORT:
Gui, add, edit, -E0x200  +center xp+50 h22 w160 ReadOnly vPathReport,
Gui, add, button, -TabStop xp+170 w70 h22, Find Report
Gui, add, text, x5 yp+27 BackgroundTrans, CP:
Gui, add, edit, -E0x200  +center xp+50 h22 w160 ReadOnly vPathCP,
Gui, add, button, -TabStop xp+170 w70 h22, Find CP
Gui, add, text, x5 yp+26 BackgroundTrans, SSAA 1:
Gui, add, edit, -E0x200  +center xp+50 h22 w160 ReadOnly vPathSSA1,
Gui, add, button, -TabStop xp+170 w70 h22, Find SSAA 1
Gui, add, text, x5 yp+26 BackgroundTrans, SSAA 2:
Gui, add, edit, -E0x200  +center xp+50 h22 w160 ReadOnly vPathSSA2,
Gui, add, button, -TabStop xp+170 w70 h22, Find SSAA 2


Gui, Show, x%winX% y%winY% Autosize, Philman %Version%
Menu, tray, NoStandard
Menu, tray, add, Exit, guiclose
return

ButtonFindReport:
FileSelectFile, ReportNew, 3, , Choose the Solmetric Report pdf for this design, Report (*.pdf)
If (ReportNew = "")
	ReportNew := Report ;Keep the original if the prompt is canceled
Else
	{Report := ReportNew
	GuiControl,, PathReport, %ReportNew%
	}
return

ButtonFindCP:
FileSelectFile, CPNew, 3, , Choose the Customer Packet pdf for this design, CP (*.pdf)
If (CPNew = "")
	CPNew := CP ;Keep the original if the prompt is canceled
Else
	{CP := CPNew
	GuiControl,, PathCP, %CPNew%
	}
return

ButtonFindSSAA1:
FileSelectFile, SSAA1New, 3, , Choose the first SSAA pdf for this design, SSAA (*.pdf)
If (SSAA1New = "")
	SSAA1New := SSAA1 ;Keep the original if the prompt is canceled
Else
	{SSAA1 := SSAA1New
	GuiControl,, PathSSA1, %SSAA1New%
	}
return

ButtonFindSSAA2:
FileSelectFile, SSAA2New, 3, , Choose the second SSAA pdf for this design, SSAA (*.pdf)
If (SSAA2New = "")
	SSAA2New := SSAA2 ;Keep the original if the prompt is canceled
Else
	{SSAA2 := SSAA2New
	GuiControl,, PathSSA2, %SSAA2New%
	}
return

/*
Gets just the filename, without extension
FileSelectFile, SSAAName, 3, , Choose the first SSAA pdf for this design, SSAA (*.pdf)
If (SSAAName = "")
	SSAAName = % SubStr(A_ScriptName, 1, -4)
Else
	{SplitPath, SSAAName,,FilePath
	SplitPath, SSAAName,,,,SSAAName
	Guicontrol,,PathSSA1,%SSAAName%
	}
*/

^r::
reload
return

^q::
guiclose:
WinGetPos, winX, winY,
IniWrite, %winX%, %A_MyDocuments%\PhilmanSettings.ini, Main, xPos
IniWrite, %winY%, %A_MyDocuments%\PhilmanSettings.ini, Main, yPos
ExitApp
return
exitapp
return